﻿
using System.IO;
using System.Diagnostics;
using System.Web.Script.Serialization;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;

namespace EscapeDollarAccntActivationMail
{
    public class MailUrlModel
    {
        public string firstName;
        public string email;
        public string aUrl;
    }

    public class Functions
    {


        private static string SendGrid_API_Key = CloudConfigurationManager.GetSetting("SendGrid_API_Key");
        private static string mailFrom = CloudConfigurationManager.GetSetting("mailFrom");

        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public async void ProcessQueueMessage([QueueTrigger("sendaccountactivateurl")] string message, TextWriter log)
        {
            Trace.TraceInformation("Processing SendAccountActivateUrl");
            // deserialize the model
            var serializer = new JavaScriptSerializer();
            var m= serializer.Deserialize<MailUrlModel>(message);

            var client = new SendGridClient(SendGrid_API_Key);
            var from = new EmailAddress(mailFrom);
            var subject = "Customer Account Activation";
            var to = new EmailAddress(m.email, m.firstName);

            string path;
            if (Environment.GetEnvironmentVariable("HOME") != null)
            {
                path = Environment.GetEnvironmentVariable("HOME") + @"\site\wwwroot";
            }
            else
            {
                path = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            }

            string plainTextBody = System.IO.File.ReadAllText(path + @"\MailTemplates\SendAccountActivation.txt");
            plainTextBody = plainTextBody.Replace(@"##fname##", m.firstName);
            plainTextBody = plainTextBody.Replace(@"##url##", m.aUrl);

            string htmlBody = System.IO.File.ReadAllText(path + @"\MailTemplates\SendAccountActivation.html");

            htmlBody = htmlBody.Replace(@"##fname##", m.firstName);
            htmlBody = htmlBody.Replace(@"##url##", m.aUrl);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextBody, htmlBody);
            var response = await client.SendEmailAsync(msg);

            Console.WriteLine("Procesed SendAccountActivateUrl for " + m.email);
        }
    }
}
